﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;

namespace Ejercicio2.Migrations
{
    public partial class foreingkeys41 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
           
            migrationBuilder.DropColumn("keyUser", "users");
            migrationBuilder.AddColumn<Guid>(name: "keyUser", table: "users", nullable: true, type: "int");

        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}
