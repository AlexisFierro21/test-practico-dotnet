﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Ejercicio2.Migrations
{
    public partial class foreingkeys : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "profile",
                table: "users",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateTable(
                name: "profile",
                columns: table => new
                {
                    id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    name = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_profile", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "workshift",
                columns: table => new
                {
                    id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    name = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_workshift", x => x.id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_users_profile",
                table: "users",
                column: "profile");

            migrationBuilder.CreateIndex(
                name: "IX_users_workshift",
                table: "users",
                column: "workshift");

            migrationBuilder.AddForeignKey(
                name: "FK_users_profile_profile",
                table: "users",
                column: "profile",
                principalTable: "profile",
                principalColumn: "id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_users_workshift_workshift",
                table: "users",
                column: "workshift",
                principalTable: "workshift",
                principalColumn: "id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_users_profile_profile",
                table: "users");

            migrationBuilder.DropForeignKey(
                name: "FK_users_workshift_workshift",
                table: "users");

            migrationBuilder.DropTable(
                name: "profile");

            migrationBuilder.DropTable(
                name: "workshift");

            migrationBuilder.DropIndex(
                name: "IX_users_profile",
                table: "users");

            migrationBuilder.DropIndex(
                name: "IX_users_workshift",
                table: "users");

            migrationBuilder.DropColumn(
                name: "profile",
                table: "users");
        }
    }
}
