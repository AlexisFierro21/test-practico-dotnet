﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Ejercicio2.Migrations
{
    public partial class foreingkeys2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "gender",
                columns: table => new
                {
                    id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    name = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_gender", x => x.id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_users_gender",
                table: "users",
                column: "gender");

            migrationBuilder.AddForeignKey(
                name: "FK_users_gender_gender",
                table: "users",
                column: "gender",
                principalTable: "gender",
                principalColumn: "id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_users_gender_gender",
                table: "users");

            migrationBuilder.DropTable(
                name: "gender");

            migrationBuilder.DropIndex(
                name: "IX_users_gender",
                table: "users");
        }
    }
}
