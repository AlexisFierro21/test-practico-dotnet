﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace Ejercicio2.Models
{
    public enum Gender { 
    masculino=1,
    femenino
    }

    [Index(nameof(pin), IsUnique = true)]
    public class User
    {
        [Key]
        public int keyUser { get; set; }
        [StringLength(30)]
        [Required]
        
        public string name { get; set; }
        [StringLength(30)]
        [Required]
        public string lastname {get;set;}
        [StringLength(30)]
        [Required]
        public string surname { get; set; }
        [StringLength(30)]
        [Required]
        public string rfc { get; set; }
        
        [StringLength(10)]
        [Required]
        [Phone]
        public string phone { get; set; }
        [StringLength(30)]
        [Required]
        public string pin { get; set; }
        [DataType(DataType.Date)]
        [Required]
        public DateTime birthdate { get; set; }

        [Required]
        [Display(Name ="workshift")]
        public virtual int workshift { get; set; }
        [ForeignKey("workshift")]
        public virtual workshift Workshift { get; set; }
        [Display(Name = "profile")]
        public virtual int profile { get; set; }
        [ForeignKey("profile")]
        public virtual profile Profile { get; set; }
        [Display(Name = "gender")]
        [Required]
        public virtual int gender { get; set; }
        [ForeignKey("gender")]
        public virtual gender  Gender { get; set; }
    }
}
