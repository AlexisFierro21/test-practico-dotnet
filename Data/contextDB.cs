﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Ejercicio2.Models;

namespace Ejercicio2.Datos
{
    public class contextDB:DbContext
    {
  

        public contextDB(DbContextOptions<contextDB> options) : base(options)
        {

        }
        public DbSet<User> users { get; set; }
    }
}
