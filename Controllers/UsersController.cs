﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Ejercicio2.Models;
using Ejercicio2.Datos;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Text.RegularExpressions;
using Microsoft.Data.SqlClient;

namespace Ejercicio2.Controllers
{
    [Route("/api/v1/users")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        public class usrJson
        {
            public int keyUser { get; set; }
            public string name { get; set; }
            public string lastname { get; set; }
            public string surname { get; set; }
            public string rfc { get; set; }
            public string phone { get; set; }
            public string pin { get; set; }
            public string birthdate { get; set; }
            public int workshift { get; set; }
            public int profile { get; set; }
            public int gender { get; set; }

        }
        public class responsePost
        {
            public int id { get; set; }
        }
        public class getSerach
        {
            public string searchby { get; set; }
        }
        private  contextDB _context;
        public UsersController(contextDB context)
        {
            _context = context;
        }
        public Boolean isRFC(string val) {
            if (!Regex.IsMatch(val, @"\d+"))
            {
                return false;
            }
            return true;
        }
        [HttpGet]
        [Route("list")]
        public IActionResult list(object data)
        {
           // var users = _context.users.Select();
            if (String.IsNullOrEmpty(data.ToString())) {
                return BadRequest();
            }
            getSerach dataAux = JObject.Parse(data.ToString()).ToObject<getSerach>();
            string sqlGet = "Select keyUser,[name],lastname,surname,rfc from users u where ";
            string sqlRFC = "";
            string sqlName = "";
            string[] searches = dataAux.searchby.ToString().Split(",");
            List<User> usrsRet = null;
           if (searches.Length==1) {
                 usrsRet = _context.users.Where(u => u.name.Contains(searches[0]) || u.rfc.Contains(searches[0])).ToList();
                
            }
            else if (searches.Length==2) {
                usrsRet = _context.users.Where(u => u.name.Contains(searches[0]) && u.rfc.Contains(searches[1])).ToList();
            }


            return Ok(usrsRet);
        }
        
        [HttpPost]
        [Route("saveorupdate")]
        public IActionResult saveorupdate(object data)
        {
            try {
                usrJson usr = JObject.Parse(data.ToString()).ToObject<usrJson>();
                User user = new User();
                if (!Regex.IsMatch(usr.rfc,@"^([A-ZÑ\x26]{3,4}([0-9]{2})(0[1-9]|1[0-2])(0[1-9]|1[0-9]|2[0-9]|3[0-1]))((-)?([A-Z\d]{3}))?$")) {
                    return BadRequest(new { ErrorMessage = "El RFC no es valido"});
                }
                if (!Regex.IsMatch(usr.phone, @"^\d{10}$")) {
                    return BadRequest(new { ErrorMessage = "El numero telefonico no es valido"});
                }
                if (!Regex.IsMatch(usr.birthdate, @"^\d{4}-\d{2}-\d{2}$"))
                {
                    return BadRequest(new { ErrorMessage = "La fecha de nacimiento es invalida" });
                }
                User usrAux = _context.users.Where(u => u.pin.Equals(usr.pin)).FirstOrDefault();
                if (usrAux != null){
                    if (usrAux.keyUser != usr.keyUser) { 
                    return BadRequest(new { Mensaje = "El pin ya es utilizado por otro usuario" });
                    
                    }
                }


                if (usr.keyUser!=0) {
                    user = _context.users.Find(usr.keyUser);
                }             
                user.name = String.IsNullOrEmpty(usr.name) ? "" : usr.name;
                user.lastname = usr.lastname;
                user.surname = usr.surname;
                user.birthdate = DateTime.Parse(usr.birthdate);
                user.gender = usr.gender;
                user.phone = usr.phone;
                user.profile = usr.profile;
                user.workshift = usr.workshift;
                user.pin = usr.pin;
                user.rfc = usr.rfc;
                if (usr.keyUser == 0)
                {
                _context.users.Add(user);
                }
                _context.SaveChanges();

                return Ok(user.keyUser);
            }
            catch (Exception e ) {
                return BadRequest(new { ErrorMessage = e.Message});
            }

          
        }
    }
}
